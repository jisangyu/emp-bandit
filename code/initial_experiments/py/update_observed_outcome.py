import pandas as pd
import numpy as np 
import os
from sklearn.linear_model import LogisticRegression
import sys
import json
import math

os.chdir('/zfs/projects/faculty/jungho-idhlab/Eddie/emp-bandit/')


def split_test_set(df, 
                   firms_per_round,
                   cols=['datadate', 'gvkey']):
    """
        Given df (test set dataframe),
        create a list of subsampled dfs by number of firms per round
        e.g. Split df into sub-dfs of 100 rows and store into list of dfs
    """
    df = df.sort_values(by=cols).reset_index(drop=True)
    num_rounds = len(df) // firms_per_round
    print(f"Total Number of Rounds: {num_rounds}")

    df_test_rounds = []
    for i in range(num_rounds+1):
        start_row = firms_per_round * i + 1
        if i == num_rounds:
            end_row = len(df)
        else:
            end_row = firms_per_round * (i + 1)

        df_test_rounds.append(df.loc[start_row:end_row, ])
    
    return df_test_rounds

    
def run_tree_classifier(df_train, seed, size, X_vars, y_var):
    train_features = df_train[X_vars].values.tolist()
    train_targets = df_train[y_var].values.tolist()


    tree = DecisionTreeClassifier(max_depth=2, random_state=seed).fit(train_features, train_targets)

    return tree

def fit_logit_classifier(df_train, X_vars, y_var, seed, penalty="l1", solver="liblinear", max_iter=100000):
    train_X = df_train[X_vars].values.tolist()
    train_y = df_train[y_var].values.tolist()

    clf = LogisticRegression(penalty=penalty, solver=solver, max_iter=max_iter, random_state=seed).fit(train_X, train_y)
    return clf


def get_test_logit(clf, df_test, X_vars):
    """
        given classifier clf, return logit estimates of test set X features
    """
    
    test_X = df_test[X_vars].values.tolist()
    logits = clf.predict_proba(test_X)[:, 1]
    return logits


def calculate_mean_variance_matrix(df_train, X_vars):
    """
        UCB: given test set X feature values list, calculate variance matrix and mean of X
    """
    
    test_X = df_train[X_vars].to_numpy()
    test_X_bar = np.mean(test_X, axis=0)
    normalized_test_X = test_X - test_X_bar

    variance_mtrx = np.matmul(normalized_test_X.T, normalized_test_X)

    return variance_mtrx, test_X_bar


def glm_ucb(clf, test_data, X_vars, variance_mtrx, test_X_bar, alpha=0.99):
    """
        UCB: calculate upper confidence bound
        variance_mtrx, test_X_bar : variance, mean vector of test set X features
    """

    test_X = test_data[X_vars].values.tolist()
    
    # given clf and test set featrues, compute MLE
    fraud_prob_array = clf.predict_proba(test_X)[:, 1] # MLE: fraud probability 
    # print(f"Fraud Logits: {fraud_prob_array}") 

    # Compute Bonus Exploration Term
    ## container for exploration bonuses
    exploration_bonus_terms = []

    for index in range(len(test_X)):

        test_X_vec = test_X[index]
        normalized_test_X_vec = test_X_vec - test_X_bar
        
        exploration_bonus_term = np.sqrt(
            np.matmul(
                np.matmul(
                    normalized_test_X_vec, variance_mtrx
                ), normalized_test_X_vec.T
            )
        )
        
        exploration_bonus_terms.append(exploration_bonus_term)

    exploration_bonus_terms = np.array(exploration_bonus_terms)
    # print(f"Exploration Bonus {exploration_bonus_terms}")

    # Upper Confidence Bound: confidence intervals
    result = fraud_prob_array + alpha * exploration_bonus_terms

    return result


def pick_inspectee(arr_probs, n_firms):
    """
        given logits / ucb values, pick the highest n firms to inspect
    """
    
    sorted_arr_probs = np.argsort(arr_probs)
    # print(np.flip(np.sort(arr_probs)))
    result = np.flip(sorted_arr_probs[-n_firms : ]) # descending order

    return result

def run_single_round_for_ssl(clf,
                             new_test_set, 
                             X_vars,
                             pick_n_firms_per_round):
    """
        Run single round of Static SL Algorithm
        (1) computing logits for a single test set round of firms (e.g. 100 candidate firms), 
        (2) pick n firms for inspection with the highest logit values
    """
    ssl_test_logits = get_test_logit(clf, new_test_set, X_vars)
    ssl_firms_to_inspect = pick_inspectee(ssl_test_logits, pick_n_firms_per_round)
    return ssl_firms_to_inspect

def run_single_round_for_usl(clf, 
                             new_test_set, 
                             X_vars,
                             pick_n_firms_per_round):
    """
        Run single round of Updating SL Algorithm
            (1) computing logits for a single test set round of firms (e.g. 100 candidate firms), 
            (2) pick n firms for inspection with the highest logit values
            (3) update classifier, training set with ONLY the observed outcomes of the firms chosen for inspection
    """

    usl_test_logits = get_test_logit(clf, new_test_set, X_vars)
    usl_firms_to_inspect = pick_inspectee(usl_test_logits, pick_n_firms_per_round)
    return usl_firms_to_inspect

def run_single_round_for_ucb(clf,
                             existing_train_set,
                             new_test_set,
                             alpha,
                             X_vars,
                             pick_n_firms_per_round):
    """
        Run single round for GLM-UCB Algorithm
            (1) computing logits for a single test set round of firms (e.g. 100 candidate firms), 
            (2) pick n firms for inspection with the highest logit values
            (3) update classifier, training set with ONLY the observed outcomes of the firms chosen for inspection        
    """
    variance_mtrx, test_X_bar = calculate_mean_variance_matrix(existing_train_set, X_vars)
    ucb_test_logits = glm_ucb(clf, new_test_set, X_vars, variance_mtrx, test_X_bar, alpha)
    ucb_firms_to_inspect = pick_inspectee(ucb_test_logits, pick_n_firms_per_round)
    return ucb_firms_to_inspect

def update_train_set(df_old, 
                     new_test_set, 
                     inspected_firm_indices):
    """
        Given existing dataframe (df_old), new dataframe test set (new_test_set), 
        and indices of firms inspected (inspected_firm_indices),
        Update training set
        inspected_firm_indices are return values from ssl/usl_firms_to_inspect
    """
    # print(new_test_set)
    new_test_set = new_test_set.reset_index(drop=True)
    # print(new_test_set)
    new_test_set = new_test_set.loc[inspected_firm_indices, :]
    print(new_test_set)
    return pd.concat([df_old, new_test_set])


if __name__ == "__main__":
    # random seed from CLI
    print(sys.argv)
    seed = int(sys.argv[1])
    print(f"Running Experiment for seed number: {seed}")

    # load json data for sec_inspection_rate
    integer_path = "./data/out/dumps/sec_inspection_rate.json"
    with open(integer_path, 'r', encoding='utf-8') as f:
        sec_inspection_rate = json.load(f)
    # sec_inspection_rate = 0.10 # change this parameter (default is 6.14%)
    
    # load json data for list of covariates
    list_covariates_path = "./data/out/dumps/covariates_list.json"
    with open(list_covariates_path, 'r', encoding='utf-8') as f:
        covariates = json.load(f)
    
    print(f"We are using covariates {covariates}, with SEC Inspection Rate {sec_inspection_rate}")
    
    # Set Filepath, Read csv created from code/construct_data/split_data.py
    df_pre_path = "./data/out/split_data/main_maxscaled_pre.csv"
    df_post_path = "./data/out/split_data/main_maxscaled_post.csv"
    df_pre  = pd.read_csv(df_pre_path, index_col=False)
    df_post = pd.read_csv(df_post_path, index_col=False)
    df_post = df_post.reset_index(drop=True)
    print(f"Original Training set has {len(df_pre)} rows, while test set has {len(df_post)} rows")

    # Split Test Set into rounds of 100 firms to iterate through; store in df_test_rounds
    firms_per_round = 100
    df_test_rounds = split_test_set(df_post, firms_per_round)
    
    # run experiment
    pick_n_firms_per_round = int(firms_per_round * sec_inspection_rate)
    beta = 0.01 # decaying alphs
    X_vars = covariates + ['naics2d']
    y_var  = "restatement"
    baseline_clf = fit_logit_classifier(df_pre, X_vars, y_var, seed) # this takes some time (little over 1 minute)
    print(f"Baseline Classifier Trained on Initial Training Set: {baseline_clf.coef_}")

    # accuracy scores
    SSL_scores = []
    USL_scores = []
    UCB_scores = []


    # initialize training set (t=0)
    ssl_train_set, usl_train_set, ucb_train_set = df_pre, df_pre, df_pre

    # initialize baseline logit classifier
    update_clf_usl, update_clf_ucb  = baseline_clf, baseline_clf

    for round_i in range(len(df_test_rounds)):

        # new round of firms
        print("-"*100)
        df_test_round = df_test_rounds[round_i]
        print(f"New Round {round_i} Test Set: {len(df_test_round)} Rows")

        ## Static SL: pick firms without updating train set
        ## training set also does not change; it always stays df_pre
        ## clf does not change; it always stays baseline_clf
        ssl_firms_to_inspect = run_single_round_for_ssl(baseline_clf, df_test_round, X_vars, pick_n_firms_per_round)
        print(ssl_firms_to_inspect)

        ## Updating SL: pick firms WITH updating train set
        ## training set changes for firm indices in usl_firms_to_inspect
        ## clf changes; it updates after incorporating subset of df_test_round
        usl_firms_to_inspect = run_single_round_for_usl(update_clf_usl, df_test_round, X_vars, pick_n_firms_per_round)
        print(usl_firms_to_inspect)
        usl_train_set = update_train_set(usl_train_set, df_test_round, usl_firms_to_inspect)
        update_clf_usl = fit_logit_classifier(usl_train_set, X_vars, y_var, seed)
        print(usl_train_set)
        print(f"Updated SL Classifier Trained on Newly Observed Outcomes: {update_clf_usl.coef_}")


        ## GLM-UCB: pick firms WITH updating train set
        ## training set changes for firm indices in ucb_firms_to_inspect
        ## clf changes; it updates after incorporating subset of df_test_round
        
        # decaying learning rate for UCB algorithm
        alpha = math.exp(-beta * round_i)
        print(f"Decaying Alpha Rate: {alpha}")

        ucb_firms_to_inspect = run_single_round_for_ucb(update_clf_ucb,
                                                        ucb_train_set,
                                                        df_test_round,
                                                        alpha,
                                                        X_vars,
                                                        pick_n_firms_per_round)
        print(ucb_firms_to_inspect)
        ucb_train_set = update_train_set(ucb_train_set, df_test_round, ucb_firms_to_inspect)
        update_clf_ucb = fit_logit_classifier(ucb_train_set, X_vars, y_var, seed)
        print(ucb_train_set)
        print(f"GLM UCB Classifier Trained on Newly Observed Outcomes: {update_clf_ucb.coef_}")

        # Accuracy: Check if the algorithm correctly caught restatement / fraud
        SSL_accuracy = df_test_round[y_var].to_numpy()[ssl_firms_to_inspect].sum()/pick_n_firms_per_round
        USL_accuracy = df_test_round[y_var].to_numpy()[usl_firms_to_inspect].sum()/pick_n_firms_per_round
        UCB_accuracy = df_test_round[y_var].to_numpy()[ucb_firms_to_inspect].sum()/pick_n_firms_per_round
        SSL_scores.append(SSL_accuracy)
        USL_scores.append(USL_accuracy)
        UCB_scores.append(UCB_accuracy)

        # Done 1 round for SSL, USL, UCB!
        print(f"Completed Round {round_i} for SSL, USL, UCB")
        print(f"Updating Training Set length: {len(ucb_train_set)}")


    # output filepath for scoreboard (SSL, USL, UCB)
    outpath = f"./out/observed_outcomes/scores/scoreboard_catch{pick_n_firms_per_round}_seed{seed}.csv"

    scoreboard = pd.DataFrame({'SSL': SSL_scores, 
                               'USL': USL_scores,
                               'UCB': UCB_scores})
    print(scoreboard)
    scoreboard.to_csv(outpath, index=False)