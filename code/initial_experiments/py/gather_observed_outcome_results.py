import pandas as pd
import numpy as np 
from matplotlib import pyplot as plt
import os
import sys
import json
import math

# Direction: bash code/bash/observed_outcome/gather_observed_outcome.sh from Eddie/emp-bandit root directory

if __name__ == "__main__":
    pick_n_firms = int(sys.argv[1]) # default rate of inspection (10%, 15%)
    plot_avg_window = True if sys.argv[2]=="T" else False
    print(f"Plotting Averaged Window? (T/F): {plot_avg_window}")

    window_length = int(sys.argv[3]) # 3-round windoweed average

    tic_interval = 10 # x-axis tick every 25 rounds

    # gather seed 1~250
    dfs = []
    for seed in range(1, 251):
        df = pd.read_csv(f"./out/observed_outcomes/scores/scoreboard_catch{pick_n_firms}_seed{seed}.csv", index_col=False)
        dfs.append(df)

    print(len(dfs))

    # Assuming you have your list of dataframes called "dfs"
    # Calculate element-wise mean and standard deviation using numpy
    dfs_arrays = [df.values for df in dfs]  # Convert each dataframe to a numpy array
    print(len(dfs_arrays))
    elementwise_mean = np.mean(dfs_arrays, axis=0)  # Calculate the mean across all dataframes
    elementwise_std = np.std(dfs_arrays, axis=0)    # Calculate the standard deviation across all dataframes
    # print("-"*100)
    # print(elementwise_mean)
    # print(elementwise_std)
    # Create new dataframes for the mean and std
    mean_df = pd.DataFrame(elementwise_mean, columns=dfs[0].columns)
    std_df = pd.DataFrame(elementwise_std, columns=dfs[0].columns)

    print(mean_df)
    print(std_df)


    # Define the rounds (x-axis values)
    rounds = range(1, len(df)+1)

    # Create a Matplotlib figure and axis
    fig, ax = plt.subplots(figsize=(20, 6))

    # Loop through the columns "SSL", "USL", and "UCB"
    columns = ["SSL", "USL", "UCB"]
    colors = ["blue", "green", "red"]
    labels = ["SSL", "USL", "UCB"]


    if plot_avg_window:

        for col, color, label in zip(columns, colors, labels):
            # Extract mean values for the current column
            mean_values = mean_df[col]
            
            # Calculate the upper and lower bounds for the confidence interval
            std_values = mean_df[col].rolling(window=window_length).std()  # Calculate rolling standard deviation
            upper_bound = mean_values + 1.96 * std_values  # 95% confidence interval
            lower_bound = mean_values - 1.96 * std_values  # 95% confidence interval
            
            # Plot the mean values as a line
            ax.plot(rounds, mean_values, label=label, color=color)
            
            # Fill between the upper and lower bounds for the confidence interval
            ax.fill_between(rounds, lower_bound, upper_bound, color=color, alpha=0.2)

            plt_title = f"{window_length}-Window Averaged Values with 95% Confidence Interval"
            plot_filepath = f"./out/observed_outcomes/plt_catch{pick_n_firms}_window{window_length}.png"

    else:

        for col, color, label in zip(columns, colors, labels):
            # Extract mean and standard deviation values for the current column
            mean_values = mean_df[col]
            std_values = std_df[col]
            
            # Calculate the upper and lower bounds for the confidence interval
            upper_bound = mean_values + 1.96 * std_values  # 95% confidence interval
            lower_bound = mean_values - 1.96 * std_values  # 95% confidence interval
            
            # Plot the mean values as a line
            ax.plot(rounds, mean_values, label=label, color=color)
            
            # Fill between the upper and lower bounds for the confidence interval
            ax.fill_between(rounds, lower_bound, upper_bound, color=color, alpha=0.2)

            plt_title = "Mean and 95% Confidence Interval"
            plot_filepath = f"./out/observed_outcomes/plt_catch{pick_n_firms}.png"

    # Set labels and title
    ax.set_xlabel("Rounds")
    ax.set_ylabel("Values")
    ax.set_title(plt_title)

    # Set x-axis ticks labeled every 25 rounds
    ax.set_xticks(range(1, len(df)+1, tic_interval))

    # Add a legend
    ax.legend()
    
    # Save the Matplotlib plot as an image (e.g., PNG)
    plt.savefig(plot_filepath)

    # Show the plot
    plt.show()

    # ################################################################################
    # # 3-day window average
    # ################################################################################
    # window_length = 3

    # # Define the rounds (x-axis values)
    # rounds = range(1, 231)

    # # Create a Matplotlib figure and axis
    # fig, ax = plt.subplots(figsize=(20, 6))

    # # Loop through the columns "SSL", "USL", and "UCB"
    # columns = ["SSL", "USL", "UCB"]
    # colors = ["blue", "green", "red"]
    # labels = ["SSL", "USL", "UCB"]

    # for col, color, label in zip(columns, colors, labels):
    #     # Extract mean values for the current column
    #     mean_values = mean_df[col]
        
    #     # Calculate the upper and lower bounds for the confidence interval
    #     std_values = mean_df[col].rolling(window=window_length).std()  # Calculate rolling standard deviation
    #     upper_bound = mean_values + 1.96 * std_values  # 95% confidence interval
    #     lower_bound = mean_values - 1.96 * std_values  # 95% confidence interval
        
    #     # Plot the mean values as a line
    #     ax.plot(rounds, mean_values, label=label, color=color)
        
    #     # Fill between the upper and lower bounds for the confidence interval
    #     ax.fill_between(rounds, lower_bound, upper_bound, color=color, alpha=0.2)

    # # Set labels and title
    # ax.set_xlabel("Rounds")
    # ax.set_ylabel("Values")
    # ax.set_title("3-Window Averaged Values with 95% Confidence Interval")

    # # Set x-axis ticks labeled every 25 rounds
    # ax.set_xticks(range(1, 231, 25))

    # # Add a legend
    # ax.legend()

    # # Save the Matplotlib plot as an image (e.g., PNG)
    # plot_filepath = f"./out/observed_outcomes/plt_catch6_window{window_length}.png"
    # plt.savefig(plot_filepath)

    # # Show the plot
    # plt.show()