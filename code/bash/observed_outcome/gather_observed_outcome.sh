#!/bin/bash

module load python
declare -i window=3
echo $window
for inspection_rate in 6 10 15
do
    for bool in "T" "F"
    do
        echo python code/initial_experiments/py/gather_observed_outcome_results.py ${inspection_rate} $bool $window
        python code/initial_experiments/py/gather_observed_outcome_results.py ${inspection_rate} $bool $window
    done
done