#!/bin/bash

#SBATCH -J ucb
#SBATCH -p normal
#SBATCH --array=1-250
#SBATCH -c 1
#SBATCH --mem 1G
#SBATCH -t 2879:00
#SBATCH -o ./temp/observed_outcome/obs-outcome-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load python
echo python code/initial_experiments/py/update_observed_outcome.py ${SLURM_ARRAY_TASK_ID}
python code/initial_experiments/py/update_observed_outcome.py ${SLURM_ARRAY_TASK_ID}