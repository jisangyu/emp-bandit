import pandas as pd
import numpy as np 
import os
from sklearn.metrics import zero_one_loss
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import RobustScaler, MaxAbsScaler, MinMaxScaler
import math
import json

# set wd
os.chdir('/zfs/projects/faculty/jungho-idhlab/Eddie/emp-bandit/')
print(os.getcwd())


# .2f floating display option
pd.set_option('display.float_format', lambda x: '%.2f' % x)


# import data
dataPath = "./data/out/compa_res_fraud_secinv.csv"
df = pd.read_csv(dataPath, index_col=False)
df['datadate'] = pd.to_datetime(df['datadate'], infer_datetime_format=True)
df['year'] = df.datadate.apply(lambda x: x.year)


def digit_classify(digit):
    '''
       Function that extracts 2 digit from 6 digit NAICS/SIC code
    '''
    if digit // 10000 == 0:
        return digit // 100
    elif digit // 1000000 == 0:
        return digit // 10000
    else:
        return np.nan


    

# set variables
df['naics2d'] = df['naics'].apply(lambda x: digit_classify(x))
df.sort_values(by=['gvkey', 'datadate'], inplace=True)
df['sale_l1'] = df.groupby('gvkey')['sale'].shift(1)
df['sale_l1'] = df['sale_l1'].where(df.groupby('gvkey').fyear.diff()==1, np.nan)
df['sales_growth'] = df['sale'] / df['sale_l1'] - 1
df['mktcap'] = df['csho'] * df['prcc_c']
df['mb'] = df['mktcap'] / df['bkvlps']
df[['gvkey', 'datadate', 'sale', 'sale_l1', 'sales_growth', 'bkvlps', 'mktcap', 'mb']]


# covariates of interest (for pilot experiment)
covariates = ['at', 'capx', 'ceq', 'ch', 'dlc', 'dp', 'ebit', 'gp', 'intan', 'invt', 'lt', 'ni',
        'ppent', 're', 'mb', 'sales_growth'] # 16 compustat raw variables

# replace Inf values with NA
df.replace([np.inf, -np.inf], np.nan, inplace=True)
df = df.dropna(subset=covariates)
df = df.reset_index(drop=True)

# Scale df with MaxAbsScaler
X = df[covariates].to_numpy()
transformer = MaxAbsScaler().fit(X)
X_scaled = pd.DataFrame(transformer.transform(df[covariates]), columns=covariates)
df_scl = pd.concat([df.iloc[:, :16], X_scaled, df[['naics2d']]], axis=1) # firm unique identifiers + scaled variables : total 17 covariates (including naics2)

# Observe SEC Inspection Rate by Year
sec_inspection_rate = df_scl['sec_inv'].mean()
print(f"SEC inspects about {np.round(100*sec_inspection_rate, 2)}% of all firms")



############################
### Split data and save ####
############################
if __name__ == "__main__":
    # Set Savepath
    df_pre_path = "./data/out/split_data/main_maxscaled_pre.csv"
    df_post_path = "./data/out/split_data/main_maxscaled_post.csv"
    
    # Split
    split_year = 2012
    df_pre = df_scl[df_scl['fyear'] < split_year]
    df_post = df_scl[df_scl['fyear'] >= split_year] 
    print(f"Total Sample Size {len(df)} | Training Set Sample Size {len(df_pre)} | Test Set Sample Size {len(df_post)}")

    # Save df_pre, df_post
    df_pre.to_csv(df_pre_path, index=False)
    df_post.to_csv(df_post_path, index=False)
    
    # save sec inspection rate integer
    integer_path = "./data/out/dumps/sec_inspection_rate.json"
    with open(integer_path, 'w') as f:
        json.dump(sec_inspection_rate, f)
        
    # save covariates list to json
    list_covariates_path = "./data/out/dumps/covariates_list.json"
    with open(list_covariates_path, 'w') as f:
        json.dump(covariates, f)