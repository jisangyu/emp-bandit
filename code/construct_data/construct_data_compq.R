################################################################################
## Script      : construct_data_compq.R
## Name        : Eddie Yu
## Description : This script constructs dataframe by merging audit data to 
##               compustat quarterly via CIK-gvkey linking table, cleans values
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(readxl)

## Set working directory, data paths
setwd('/home/users/jisangyu/zfs/projects/faculty/jungho-idhlab/Eddie/emp-bandit')
auditPath = "./data/raw/auditanal_091423.csv"
cikLinkPath = "./data/raw/cik-cusip.csv"
compqPath = "./data/raw/compq_091723.csv"
permnoLinkPath = "./data/raw/lpermno_gvkey_cik.csv"
SECinvPath = "./data/raw/SECInvestigations.xlsx"

outPath = "./data/out/compq_res_fraud_secinv.csv"

audit = fread(auditPath)
cikLink = fread(cikLinkPath)
compq = fread(compqPath)
permnoLink = fread(permnoLinkPath)
SECinv = setDT(read_excel(SECinvPath, sheet=1))
permno_SECinv = setDT(read_excel(SECinvPath, sheet=2))

names(audit) <- tolower(names(audit))
names(cikLink) <- tolower(names(cikLink))
names(permnoLink) <- tolower(names(permnoLink)) 

# 0. Change datetime objects
datetime_cols = c("res_begin_date", "res_end_date", "file_date")  
audit[, (datetime_cols) := lapply(.SD, 
                                  \(x) as.Date(x)),
      .SDcols = datetime_cols]

datetime_cols = c("cikdate1", "cikdate2")
cikLink[, (datetime_cols) := lapply(.SD, 
                                    \(x) as.Date(as.character(x), format="%Y%m%d")),
        .SDcols = datetime_cols]

datetime_cols = c("datadate")
compq <- compq[order(gvkey, fyearq, fqtr, datadate)]
compq <- unique(compq, by = c("gvkey", "fyearq", "fqtr")) # some duplicate rows exist
compq[, (datetime_cols) := lapply(.SD, as.Date),
      .SDcols = datetime_cols]

datetime_cols = c("open_date", "close_date")
SECinv[, (datetime_cols) := lapply(.SD, as.Date), # distinct investigation_id
       .SDcols = datetime_cols]
permno_SECinv[, (datetime_cols) := lapply(.SD, as.Date), # distinct investigation_id
              .SDcols = datetime_cols]



# 1. Link compustat quarterly to permno
## 1-1. permno table: make it unique gvkey-conm-cusip-cik-lpermno identifier
## change "E" to latest permnoLink Date (2022-01-31)
sample_end_date = Sys.Date() # latest date on Compustat (to match)
sample_start_date = as.Date(as.character(min(permnoLink$linkdt)), format="%Y%m%d") # earliest date on PermnoLink
convert_E_to_LatestDate <- function(enddate) {
  return(ifelse(enddate == "E", sample_end_date, as.Date(enddate, format="%Y%m%d")))
}
permnoLink[, linkdt := as.Date(as.character(linkdt), format="%Y%m%d")]
permnoLink[, linkenddt := 
             as.Date(
               unlist(lapply(linkenddt, convert_E_to_LatestDate)),
               origin="1970-01-01"
               )]

## 1-2. index each element in group, to make the coverage of linkdt ~ linkenddt as much as possible
permnoLink = permnoLink[order(gvkey, linkdt)]
permnoLink[, key := 1:.N, by=.(gvkey)]
permnoLink[, dups := .N, by=.(gvkey)]

## 1-3. change the dates to cover the largest span of dates (to match well with compustat datadate)
permnoLink[, linkdt1 := ifelse(key==1, sample_start_date, linkdt)] # first row of gvkey group : change to earliest date
permnoLink[, linkdt1 := as.Date(linkdt1, origin="1970-01-01")]
permnoLink[, linkenddt1 := ifelse(key==dups, sample_end_date, linkenddt)] # last row of gvkey group : change to latest date
permnoLink[, linkenddt1 := as.Date(linkenddt1, origin="1970-01-01")]

## 1-4. set key for overlap join with compustat
setkeyv(permnoLink, c("gvkey", "linkdt1", "linkenddt1"))
compq[, datadate_dup := datadate]
setkeyv(compq, c("gvkey", "datadate", "datadate_dup"))

## 1-5. overlap join compustat to permnoLink
df <- foverlaps(compq, permnoLink[, .(gvkey, linkprim, linktype, 
                                      lpermno, lpermco, linkdt, linkenddt,
                                      linkdt1, linkenddt1)], 
                type="any", 
                mult="all") #1,062,746 matches out of 1,496,673 rows; 70% match
df <- df[!is.na(lpermno),]
setkeyv(df, c("gvkey", "datadate", "datadate_dup"))
rm(compq, permnoLink)


## 2. merge to SEC investigation binary indicator
permno_SECinv <- permno_SECinv[!is.na(permno)]
setkeyv(permno_SECinv, c("permno", "open_date", "close_date"))
df_SECinv <- foverlaps(df, permno_SECinv, 
                 by.x=c("lpermno", "datadate", "datadate_dup"),
                 by.y=c("permno", "open_date", "close_date"),
                 type="any", mult="all")
df_SECinv[, sec_inv := as.numeric(!is.na(open_date))] # 3.56% is investigated
df_SECinv[, yearqtr := 10*fyearq+fqtr]
df_SECinv[, yearqtr_dup := yearqtr]
df_SECinv <- df_SECinv[!is.na(yearqtr_dup),]

## 2. Merge with Audit analytics to get fraud binary indicator
audit <- audit[, .(company_fkey, res_begin_date, res_end_date, 
                   res_accounting, res_acc_res_title_list, 
                   res_fraud, res_fraud_res_fkey_list, res_fraud_res_title_list,
                   res_sec_invest, disc_text, file_date, http_name_html,
                   best_edgar_ticker)]
audit[, `:=` (yrqtr_res_begin_date =
                10*year(res_begin_date) + quarter(res_begin_date),
              yrqtr_res_end_date = 10*year(res_end_date) + quarter(res_end_date))]
setkeyv(audit, c("company_fkey", "yrqtr_res_begin_date", "yrqtr_res_end_date"))

final <- foverlaps(df_SECinv, audit,
                 by.x = c("cik", "yearqtr", "yearqtr_dup"),
                 by.y = c("company_fkey", "yrqtr_res_begin_date", "yrqtr_res_end_date"),
                 type="any",
                 mult="first") # only 71,253 out of 1,066,678 rows are restatements (6.68%)
final[, `:=` (restatement = as.numeric(!is.na(res_begin_date)),
              res_fraud = ifelse(is.na(res_fraud), 0, res_fraud))] # 0.2232% is fraud; 2380 fraud cases out of 71,253 restatements (3%)

final[, `:=` (indfmt = NULL,
              consol = NULL,
              popsrc = NULL,
              datafmt = NULL,
              curcdq = NULL,
              datadate_dup = NULL,
              yearqtr_dup = NULL,
              yrqtr_res_begin_date = NULL,
              yrqtr_res_end_date = NULL,
              linkprim = NULL,
              linktype = NULL,
              matter_no = NULL,
              open_date = NULL,
              close_date = NULL)]

setcolorder(final, c("gvkey", "datadate", "fyearq", "fqtr", "yearqtr",
                     "tic", "cusip", "cik", "conm", "conml",
                     "sec_inv", "restatement", "res_begin_date", "res_end_date",
                     "res_accounting", "res_acc_res_title_list", "res_fraud",
                     "res_fraud_res_fkey_list", "res_fraud_res_title_list",
                     "res_sec_invest", "disc_text", "file_date"))

fwrite(final, outPath)